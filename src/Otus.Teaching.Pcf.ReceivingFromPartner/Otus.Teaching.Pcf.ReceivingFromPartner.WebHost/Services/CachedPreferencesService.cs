﻿using Microsoft.Extensions.Caching.Distributed;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Services
{
    public class CachedPreferencesService : IPreferencesService
    {
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IDistributedCache _preferencesCache;

        public CachedPreferencesService(IRepository<Preference> preferencesRepository, IDistributedCache preferencesCache)
        {
            _preferencesRepository = preferencesRepository;
            _preferencesCache = preferencesCache;
        }

        public async Task<IEnumerable<Preference>> GetAllAsync()
        {
            var cacheKey = "preference_all";

            var preferencesFromCache = await _preferencesCache.GetAsync(cacheKey);
            if (preferencesFromCache != null)
            {
                string serializedPreferences = Encoding.UTF8.GetString(preferencesFromCache);
                return JsonSerializer.Deserialize<IEnumerable<Preference>>(serializedPreferences);
            }

            var preferences = await _preferencesRepository.GetAllAsync();
            await AddToCache(cacheKey, preferences);
            return preferences;
        }

        public async Task<Preference> GetByIdAsync(Guid id)
        {
            var cacheKey = $"preference_{id}";

            var preferenceFromCache = await _preferencesCache.GetAsync(cacheKey);
            if (preferenceFromCache != null)
            {
                string serializedPreference = Encoding.UTF8.GetString(preferenceFromCache);
                return JsonSerializer.Deserialize<Preference>(serializedPreference);
            }

            var preference = await _preferencesRepository.GetByIdAsync(id);
            await AddToCache(cacheKey, preference);
            return preference;
        }

        private async Task AddToCache(string cacheKey, object preferences)
        {
            string serializedPreferences = JsonSerializer.Serialize(preferences);
            var cacheValue = Encoding.UTF8.GetBytes(serializedPreferences);
            var cacheOptions = new DistributedCacheEntryOptions()
                .SetAbsoluteExpiration(DateTime.Now.AddHours(10))
                .SetSlidingExpiration(TimeSpan.FromHours(1));

            await _preferencesCache.SetAsync(cacheKey, cacheValue, cacheOptions);
        }
    }
}
