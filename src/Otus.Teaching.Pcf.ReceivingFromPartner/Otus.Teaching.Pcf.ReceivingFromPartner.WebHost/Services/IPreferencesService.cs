﻿using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Services
{
    public interface IPreferencesService
    {
        Task<IEnumerable<Preference>> GetAllAsync();

        Task<Preference> GetByIdAsync(Guid id);
    }
}
